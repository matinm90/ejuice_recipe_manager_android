package mehdipour.matin.eliquiddiary;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by matt on 4/28/15.
 */
public class RecipeListFragment extends Fragment
{
    View v;
    EliquDBHelper eliquDBHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        v = inflater.inflate(R.layout.recipe_list_frag_layout, container, false);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        eliquDBHelper = new EliquDBHelper(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.recipe_list_frag_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
       if(item.getItemId() == R.id.add_recipe_menu)
       {
           eliquDBHelper.add();
           return true;
       }
        return false;
    }

    private void showAddRecipeDialog()
    {

    }
}
